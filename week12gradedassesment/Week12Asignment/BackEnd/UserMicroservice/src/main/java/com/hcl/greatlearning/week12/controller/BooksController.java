package com.hcl.greatlearning.week12.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.greatlearning.week12.bean.Books;
import com.hcl.greatlearning.week12.service.BooksService;

@RestController
@RequestMapping("/books")
@CrossOrigin
public class BooksController {
	@Autowired
	BooksService booksService;
	
	@GetMapping(value = "getAllBooks",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllBooksInfo() {
		return booksService.getAllBooks();
	}
}
