package com.hcl.greatlearning.week12.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.greatlearning.week12.bean.Admin;
import com.hcl.greatlearning.week12.dao.AdminDao;

@Service
public class AdminService {
   @Autowired
   AdminDao adminDao;
   
   public String register(Admin admin) {
	   Admin a=adminDao.findByAdminnameAndPassword(admin.getAdminname(), admin.getPassword());
	   if(Objects.nonNull(a)) {
		   return "Admin name already exist";
	   }else {
		   adminDao.save(admin);
		   return"Admin Details Registered Successfully";
	   }
	   
   }
   public String loginAdmin(Admin admin) {
	   Admin ad=adminDao.findByAdminnameAndPassword(admin.getAdminname(), admin.getPassword());
	   if(Objects.nonNull(ad)) {
		   return"Login Successfully";
	   }else {
		   return"Invalid Adminname or password";
	   }
   }
   }
   